import 'dart:html';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  final _formKey = GlobalKey<FormState>();


void getHttp() async {
    // try {
//      Response response = await Dio().post(
//        "https://80140596.ngrok.io/api/login",
//        data: {
//          "email": "admin@admin.com",
//          "password": "123456",
//        }
//      );

     Map<String, dynamic> inputMap = new Map();

    // for (var input in inputs) {
    //   inputMap[input.attributes['name']] = input.attributes['value'];
    // }
  
    var dio = new Dio();
    inputMap['_token'] = "wfyZDctHLrNAkCfnPRQxEvwJiJyIGpVoXLGp7fjm";
    print('inputMap $inputMap');
    try{
      var r = await dio.post("http://digidinos.com/login",
      data: inputMap,
      options: Options(
          followRedirects: false,
          validateStatus: (status) { return status < 500; }
      ),
      );
      print(r);
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: new Form(
        key: _formKey,
        child:   Center(
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 16.0,horizontal: 10.0),
                child: RaisedButton(
                  onPressed: () {
                    getHttp();
                  },
                  child: Text('Login'),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
