
import 'package:flutter/material.dart';
import 'package:english_words/english_words.dart';
import 'package:google_fonts/google_fonts.dart';
void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Shopping',
      theme: ThemeData(
        primarySwatch: Colors.green
      ),
      home: new RandomEnglishWords(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

//StatefulWidget
class RandomEnglishWords extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new RandomEnglishWordsState();
  }
}

//State
class RandomEnglishWordsState extends State<RandomEnglishWords> {
  final _word = <WordPair>[];
  final _checkedWords = new Set<WordPair>();// set contains "no duplicate items"

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(
          "List of English words", 
          style: GoogleFonts.montserrat()),
      ),
      body: new ListView.builder(itemBuilder: (context, index){
        // This is an anonymous function
        // index = 0, 1, 2, 3...
        // This function return each Row = "a Widget"
        if(index >= _word.length){
          _word.addAll(generateWordPairs().take(10));
        }
        return _buildRow(_word[index],index);
      }),
    );
  }
  Widget _buildRow(WordPair wordPair, int index){
  //This widget is for each row
  final color = index % 2 == 0 ? Colors.green : Colors.blue;
  final isChecked = _checkedWords.contains(wordPair);
  return new ListTile(
    // leading = left , traililng = right. Is is correct ? => Not yet
    leading: new Icon(
      isChecked ? Icons.check_box : Icons.check_box_outline_blank,
      color: color,
      ),
    title: new Text( 
      wordPair.asCamelCase,
      style: new TextStyle(fontSize: 18.0, color: color),
    ),
    onTap: () {
       setState(() {
         if(isChecked) { 
           _checkedWords.remove(wordPair); //Remove item in a Set 
         }else {
           _checkedWords.add(wordPair); // Add item to a Set
         }
       });
    },
  );
}
}





class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return new RandomEnglishWords();
  }
}
