import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:html/parser.dart' show parse;

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.teal
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

Future<void> getHttp() async {
   Map<String, dynamic> inputMap = new Map();
   var dio = new Dio();
  //  inputMap['email'] = 'hieu.hm@digidinos.com';
  //  inputMap['password'] = 'digidinos';
  //  inputMap['_token'] = 'wfyZDctHLrNAkCfnPRQxEvwJiJyIGpVoXLGp7fjm';
    var document = await dio.get(
      "http://digidinos.com/login",
      );
      var dom = parse(document.data.toString());
      var x = dom.querySelectorAll('input');
      for(var item in x){
        inputMap[item.attributes['name']] = item.attributes['value'];
      }

      inputMap['email'] = "hieu.hm@digidinos.com";
      print(inputMap);
   try{
     var r = await dio.post(
      "http://digidinos.com/login",
      data: inputMap,
      options: Options(
        followRedirects: false,
        validateStatus: (status) => status < 500
      ),
     );
    print(r);
   }catch(e){
     print(e);
   }
 }

class _MyHomePageState extends State<MyHomePage> {
final _formKey = GlobalKey<FormState>();


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: new Form(
        key: _formKey,
        child: Center(
          child: Column(
            children: <Widget>[
              new RaisedButton(
                onPressed: getHttp,
                child: Text("Login"),
                ),
                
            ],
          ),
        )
      
      )
    );
  }
}
