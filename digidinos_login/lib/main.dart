import 'package:flutter/material.dart';
import 'package:dio/dio.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Digi Dinos Login',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyLoginForm(title: 'Login'),
    );
  }
}

class MyLoginForm extends StatefulWidget {
  MyLoginForm({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyLoginFormState createState() => _MyLoginFormState();
}

class _MyLoginFormState extends State<MyLoginForm> {
  //Create a global key that uniquely identifies the Form widget
  //and allows validation of the form
  //
  //Note: This is a GlobalKey<FormState>,
  //not a GlobalKey<MyCustomFormState>.
  final _formKey = GlobalKey<FormState>();
  void getHttp() async {
//    FormData formData = FormData.fromMap({
//      "email": "hieu.hm@digidinos.com",
//      "password": "digidinos",
//      "_token": "29hmEmvvK0l95YcfWKRDNR3LZzK8zPoZhsZv7WrD",
//    });
//    var client = Dio(BaseOptions(baseUrl: "https://80140596.ngrok.io/api/login"));
    try {
//      Response response = await Dio().post(
//        "https://80140596.ngrok.io/api/login",
//        data: {
//          "email": "admin@admin.com",
//          "password": "123456",
//        }
//      );

      Map<String, dynamic> data = new Map<String, dynamic>();
      data['email'] = "hieu.hm@digidinos.com";
      data['_token'] = "wfyZDctHLrNAkCfnPRQxEvwJiJyIGpVoXLGp7fjm";

      var r  = await Dio().post(
        "http://digidinos.com/login",
//        data: {
//          "email": "hieu.hm@digidinos.com",
//          "password": "digidinos",
//          "_token": "wfyZDctHLrNAkCfnPRQxEvwJiJyIGpVoXLGp7fjm"
//        },
        data: data,
        options: Options(
            followRedirects: true,
            contentType:Headers.formUrlEncodedContentType ,
            validateStatus: (status) { return status < 500; }
        ),
      );
      print(r);
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        centerTitle: true,
      ),
      body:
      new Form(
        key: _formKey,
        child:   Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              TextFormField(
//                validator: (value) {
//                  if(value.isEmpty) {
//                    return 'Please enter some text';
//                  }
//                  return null;
//                },
                decoration: InputDecoration(hintText: "User name"),
              ),
              TextFormField(
//                validator: (value) {
//                  if(value.isEmpty) {
//                    return 'Please enter some text';
//                  }
//                  return null;
//                },
                decoration: InputDecoration(hintText: "Password"),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 16.0,horizontal: 10.0),
                child: RaisedButton(
                  onPressed: () {
                    getHttp();
                  },
                  child: Text('Login xxx'),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
